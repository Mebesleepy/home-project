from flask import Flask 
from flask_restful import Api, Resource, fields, marshal_with, abort, reqparse 
from flask_sqlalchemy import SQLAlchemy
from passwordManegment import hash_password, check_password
from datetime import timedelta, datetime
import jwt as jwt
import confuse

config = confuse.Configuration('home-project', __name__)
db_path = config['db']['path'].get()
secret = config['secret']['secret_key'].get()

app = Flask(__name__)
api = Api(app)
db = SQLAlchemy(app)

# TODO: change this to a proper secret key...that is secret.
app.config['SECRET_KEY'] = secret 
app.config['SQLALCHEMY_DATABASE_URI'] = db_path


user_put_args = reqparse.RequestParser()
user_put_args.add_argument("username", type=str, help="username is requiered", required=True)
user_put_args.add_argument("password", type=str, help="password is requiered", required=True)

user_update_args = reqparse.RequestParser()
user_update_args.add_argument("username", type=str, help="check username requirements")
user_update_args.add_argument("password", type=str, help="check password requirements")

login_args = reqparse.RequestParser()
login_args.add_argument("username", type=str, help="check username requirements")
login_args.add_argument("password", type=str, help="check password requirements")

class UserModel(db.Model):
    id = db.Column(db.Integer, primary_key =True)
    username = db.Column(db.String(100), nullable=False)
    key = db.Column(db.String, nullable=False)
    salt = db.Column(db.String, nullable=False)


# def __repr__(self):
    # return f"User: username={username}, password={password}"

# db.create_all() # only set up once

# we need to serialize the output and say here how we want it to look
resource_fields = {
    'id' : fields.Integer,
    'username': fields.String,
    'salt': fields.String,
    'key': fields.String
}

class User(Resource):
    @marshal_with(resource_fields)
    def get(self, user_id):
        result = UserModel.query.filter_by(id=user_id).first()
        if not result:
            abort(404, message=f"This User with id {user_id} was not found")
        return result, 200


    @marshal_with(resource_fields)
    def put(self):
        args = user_put_args.parse_args()
        result = UserModel.query.filter_by(username=args['username']).first()
        if result:
            abort(409, message=f"The user with username: '{args['username']}' already exists")
    
        salt_and_key = hash_password(args['password'])

        user = UserModel(username=args['username'], key=salt_and_key['key'], salt=salt_and_key['salt'])
        db.session.add(user) # add as pending
        db.session.commit() # commit all pending additions or deletions that is pendingg
        return user, 201
    

    @marshal_with(resource_fields)
    def patch(self, user_id):

        args = user_update_args.parse_args()

        result = UserModel.query.filter_by(id=user_id).first()

        if not result:
            abort(404, message=f"Could not update user with id: {user_id}, was not found")

        if args['username']:
            result.username = args['username']
        if args['password']:
            new_salt_and_key = hash_password(args['password'])
            result.key = new_salt_and_key['key']
            result.salt = new_salt_and_key['salt']

        db.session.commit()

        return result, 201


    def delete(self, user_id):
        result = UserModel.query.filter_by(id=user_id).first()
        if not result:
            abort(404, message=f"Could not delete user with id: {user_id}, was not found")

        db.session.delete(result)
        db.session.commit()

        return {"message":f"Deleted user with id {user_id}"}, 200


class Login(Resource):
    def post(self):
        args = login_args.parse_args()

        if not args['username'] or not args['password']:
            abort(404, message='username and password must be provided to login')

        result = UserModel.query.filter_by(username=args['username']).first()

        # TODO: return more generic after testing, since we do not want to give to much information

        if not result:
            abort(403, message=f"Username does not exist")
        
        if not check_password(args['password'], result.salt, result.key):
            abort(403, message=f"Password does not match this user")

        session['user_logged_in'] = args['username']
        token = jwt.encode({
            'user': args['username'],
            # TODO: time is for testing purposes change to something more realistic later
            'exp': datetime.utcnow() + timedelta(seconds=60)
        }, app.config['SECRET_KEY'])

        return{'message': f"login with {args['username']} was approved", 'token': token.decode('utf-8')}
        
            
api.add_resource(User, "/user", "/user/<int:user_id>")
api.add_resource(Login, "/login")

if __name__ == "__main__":
    app.run(debug=True) # TODO: disable debug in production
