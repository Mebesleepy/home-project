def check_for_token(func):
    @wraps(func)
    def wrapped(*args, **kwargs):
        token = request.args.get('token')
        if not token:
            return abort(404, message="No token was provided");
        
        try:
            data = jwt.decode(token, app.config['SECTET_KEY'])
        except jwt.ExpiredSignature:
            return abort(404, message:"Token has expired")
        except: 
            return abort(404, message: "The token could not be verified") 
