from os import urandom
from hashlib import pbkdf2_hmac

iterations = 100000
salt_bytes = 32

def get_salt():
    return urandom(salt_bytes)


def hash_password(password, salt=get_salt()):

    key = pbkdf2_hmac(
    'sha256',
    password.encode('utf-8'),
    salt,
    iterations
)
    return {'salt': salt, 'key' : key}


def check_password(password, salt, key):

    new_key = hash_password(password, salt)

    return key == new_key['key']
